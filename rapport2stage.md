---
title: Rapport de stage
subtitle: Le Logiciel de Gestion d'Officine Chrysalide
author: Maxime Cesson
lang: fr
institute: CY Tech
documentclass: scrreprt
geometry: 
- top=24mm
- bottom=28mm
- left=20mm
- right=20mm
linestretch: 1.25
fontsize: 12pt
...

<!-- à convertir avec $ pandoc rapport2stage.md --toc -o output.pdf -->


# Remerciements

Je remercie :  

Madame **Céline Tahar Henriques** pour sa patience et ses précieuses réponses en période de changement de stage de dernière minute.  

**Céline**, **Roseline** et **ma Maman** pour m'avoir déniché ce stage en un éclair.  

**Charlène** pour la présentation de Winpharma un samedi midi à l'heure de la fermeture.  

**Nicolas**, mon tuteur, pour toutes les discussions captivantes sur le libre, la santé et autres, pour son engagement envers les autres, pour tout ce qu'il m'a appris et pour son soutien et sa gentillesse tout au long de mon stage.  

**Manu** pour son temps, son accessibilité, ses "Live Coding session" et ses présentations de son travail de développeur backend.  

**Charles** pour sa thèse sur les LGO, très accessible et très complète pour un néophyte de la pharmacie.  

L'ensemble des autres membres de P4pillon que j'ai rencontré pour leur accueil et leur volonté de transformer le monde de la pharmacie (**Antoine**, **Maxime**, **Erwan**, **Arthur**, **Célia** et **Samuel**).  

**Manon** pour les relectures et pour avoir prêté une oreille attentive à toutes mes préoccupations durant ce stage.  

Je transmets pour finir tous mes encouragements à **Luc** et **Thibaud**, les écuyers du hack, pour la suite de leur scolarité au lycée. Continuez à bidouiller, à tester, à casser, à innover, à programmer, à scripter : de chouettes métiers vous attendent et vous avez toutes les cartes en main pour les embrasser !


# Introduction

Depuis qu'ont été autorisés les stages non rémunérés à CY Tech et étant concerné par le monde associatif, l'idée d'effectuer un stage pour une association me séduisait. Secrétaire de l'association ATILLA (Association des Techniques Internet et du Logiciel Libre ou Alternatif), j'ai également eu la chance d'être senbilisé aux enjeux du logiciel libre cette année, aussi un stage dans ce cadre semblait-il idyllique.

Pourtant, le choix de ce stage en particulier s'est fait un peu par hasard. En recherche urgente de stage suite à l'annulation d'un autre stage près d'un mois après le début des démarches de signature de conventions, l'on m'a transmis un numéro à appeler "sans rien me promettre" et deux heures après je prenais part à une réunion interne à l'association P4pillon ! En effet, au début du mois de juin 2021, l'association dont l'idéal est de transformer la pharmacie d'officine profitait de la retombée médiatique d'un épisode de l'émission à succès Cash Investigation [1]. Se formait alors une équipe de bénévoles prêt à s'investir dans la réimplémentation libre d'un logiciel de gestion d'officine (LGO).

Au delà de la chance d'avoir croisé le chemin de Nicolas Floquet, mon tuteur, certains éléments que j'ai très vite retrouvé dans la description que l'on m'a fait du projet de P4pillon ont structuré ma recherche de stage initiale. D'abord, il y avait le fait de participer à un projet à relativement court terme dans une structure à taille humaine. En effet, en plus d'être compatible avec les attentes académiques, cela est très valorisant : l'on a l'impression d'apporter un réel quelque chose à l'organisation qui nous accepte en stage. De même, j'espérais travailler sur un projet concret, défini, dont on peut facilement estimer l'intérêt dans le temps, comme c'est tout à fait le cas pour le projet de P4pillon. Enfin, un dernier attrait était celui de travailler au seing d'une équipe d'ingénieurs, de professionnels de l'informatique chevronnés, avec une réelle expérience du monde de l'entreprise. Bien qu'une bonne partie de mon travail ait été effectuée en autonomie dans une association, j'ai tout de même en partie retrouvé ce point par le caractère bénévole du projet et la facilité d'accès des autres participants dont certains étaient parfaitement représentés par les qualificatifs précedemment exprimés.  

Par le caractère varié des missions effectuées durant ce stage, seront abordées dans ce rapport et dans un ordre chronologique les divers thématiques traitées, chacune d'entre elles étant contextualisée, décrite et analysée.


# L'entreprise d'accueil

Bien que ce soit l'ensemble de l'association P4pillon qui m'ai accueilli, j'ai avant tout effectué ce stage sous l'égide de la micro-entreprise de mon tuteur, Nicolas Floquet, également vice-président de P4pillon et recruteur-coordinateur de la mission de développement du LGO.


## La micro-entreprise de M. Floquet

Docteur en pharmacie de l'université de Lille II, Nicolas Floquet s'installe en Normandie en 2010 à l'issue de ses études. En 2012, il cesse d'exercer la profession de pharmacien pour se consacrer au Logiciel Libre, sujet découvert pendant ses études en pharmacie, traité dans sa thèse d'exercice [2] et incarné depuis.
En effet, il milite pour que les professionnels de santé prennent conscience qu'ils ne contrôlent aujourd'hui déjà plus les données de leurs patients et que le logiciel libre est pour eux la seule solution pour reprendre durablement le contrôle et ainsi honorer les serments qu'ils ont prêtés.  
Plus généralement, M. Floquet est un fervant défenseur du Logiciel Libre qui ne cesse de promouvoir toutes les initiatives normandes. Il est d'ailleurs co-créateur de plusieurs projets tels que le collectif "Normandie Libre" qui vise à rassembler les libristes de la région.  
De plus, Nicolas Floquet est très engagé dans le maillage associatif, en témoigne son engagement pour P4pillon mais aussi les cours bénévoles de présentation et d'utilisation de logiciels libres qu'ils dispensent au seing de l'association Syntax informatique de Domfront dont il est également trésorier.
Néanmoins, dans le but d'encadrer légalement ses activités extra-associatives présentes et à venir, il devient micro-entrepreneur dès 2013.  
Enfin, comme les autres membres de P4pillon, il travaille exclusivement de chez lui : la gestion du développement durable de l'association est donc partagée entre toutes les habitudes individuelles des membres, tout en limitant les transports et les émissions carbones.


## L'association P4pillon

L'association P4pillon, quant à elle, sur son site internet [3], capte notre attention par des slogans tels "Construisons l'avenir du système de santé" ou "Libérez P4pillon ! Et en retour il vous libère" et se définit comme association de recherche et développement en santé et en soins de premier recours. En cela, il est tout de suite à préciser que le développement d'un LGO libre n'est pas l'objectif central de P4pillon. Pour l'association, il faut voir l'implémentation de ce nouveau LGO comme un moyen plutôt qu'une fin : un moyen de faire évoluer la pharmacie, de ne pas attendre les décrets pour appliquer la médecine des 4P (personnalisée, préventive, prédictive et participative) [4], de rémunérer les pharmaciens pour les boîtes qu'ils ne vendent pas plutôt que pour celles qu'ils vendent.

Ces arguments chocs sont, entre autres, ceux portés par Antoine Prioux [5], président-fondateur de P4pillon, docteur en pharmacie de l'université de Limoges et qui exerce en Corrèze depuis sa diplomation, sur le plateau de Millevaches, là où il a grandi. Il est pharmacien en officine au seing d'une maison de santé pluriprofessionnelle universitaire de première importance pour lui puisqu'à ses yeux, il est nécessaire que l'évolution de la pharmacie s'accompagne :

* d'une coordonation dans **l'espace**, à l'échelle d'une "maison de santé" avec le partage "local" du dossier patient et la mise en place d'un pharmacien correspondant parmi une équipe de soins coordonnées; 
* d'une coordination dans **le temps** avec la mise en place de parcours de soin chronique et des prescriptions en multiples de semaines plutôt qu'en mois (la semaine étant stable, contrairement au mois qui fait tantôt 28, 29, 30 ou 31 jours).

Comme le dit lui-même Antoine Prioux, avec quatre couleurs et des barquettes en carton, il met en place ces deux principes et entend libérer du temps pharmaceutique en anticipant les parcours de soins. C'est bien en cela que s'intègre le futur LGO dans le projet P4pillon : accompagner les futurs pharmaciens dans leur changement de paradigme. 

Enfin, le dernier argument et non le moindre : la solution proposée a déjà été testée et fonctionne ! En effet, au cours des quatre dernières années, un développeur indépendant s'est joint à Antoine Prioux et ils ont construit ensemble un logiciel fonctionnel qui implémente les idées novatrices du pharmacien.
Or, ce logiciel, non libre, appartient entièrement au développeur qui l'a implémenté, et quand celui-ci a choisi de partir du projet, son logiciel l'a suivi. Aussi une campagne de financement participatif est-elle toujours en cours lors de la rédaction de ce rapport pour pouvoir racheter et libérer le code de ce LGO privateur. Cependant, l'idée de réécrire du début à la fin un nouveau LGO libre, plus beau, plus grand, plus fort, germe au milieu de l'équipe.

Ce nouveau LGO, qui devrait porter le nom de Chrysalide, comme son prédecesseur, promet ainsi de devenir un formidable outil pour les futures générations de pharmaciens conjugant élégamment les diffusions des messages ci-dessus portés par Antoine à travers P4pillon et ceux du respect du secret médical et du contrôle des informations patients, autrement dit les intérêts défendus par mon tuteur, Nicolas Floquet. 


# Ma mission, les résultats, mon analyse

## Phase de documentation

Le stage a débuté très vite après mon premier contact avec Nicolas, aussi m'a-t-il manqué beaucoup d'informations sur P4pillon et son projet lors de mes premiers jours de stage.


### Approfondissement de ma connaissance du monde du libre

Bien que pensant avoir une bonne idée de ce qu'était le monde du logiciel libre, seules quelques dizaines de minutes passées à discuter avec mon tuteur m'ont suffit à comprendre que ma connaissance se limitait à des notions - globalement ce dans quoi ATILLA est ou s'était impliqué.  

Avant tout, il est nécessaire d'avoir conscience que le logiciel libre ne se résume pas à un logiciel gratuit, la confusion venant peut-être de la traduction litérale de l'anglais "Free Software". Bien qu'il existe nombre de licences [6] dont chacune possède ses propres spécificités, un logiciel est dit "libre" quand il respecte quatre points essentiels :

* liberté d'exécuter le programme comme on le souhaite
* liberté d'étudier le code source et de le modifier selon ses désirs
* liberté de redistribuer des copies exactes du programme quand l'on veut et à qui l'on veut *(liberté d'aider)*
* liberté de diffuser des versions modifiées quand l'on veut et à qui l'on veut *(liberté de contribuer à la communauté)*

Il s'oppose à un logiciel dit "privateur", aussi connu sous le nom de logiciel "propriétaire", qui, par définition, "prive" l'utilisateur d'au moins une de ces quatre libertés fondamentales.  

Dans le but de parfaire ma connaissance du logiciel libre, Nicolas m'a fourni de nombreuses ressources. 
Il y eut de quoi me dépeindre la dimension historique du libre avec les hackers de la deuxième partie du XXe siècle [7] puis avec l'impulsion donnée par Richard Stallman au début du XXIe siècle [8] jusqu'à la manière viable, dans le contexte actuel, de développer un logiciel libre [9].
Ensuite j'ai, entre autres, pu découvrir l'e Foundation [10] ou la Brique Internet [11], qui visent à rendre internet plus respectueux envers ses utilisateurs,  respectivement grâce à /e/, un système d'exploitation déGooglisé pour mobile basé sur Android et grâce à une "brique" à brancher sur la box de son fournisseur d'accès à internet.
Enfin, et c'est peut-être le point principal, j'ai également été amené à réfléchir sur la philosophie du libre [12][13][14] et sur l'enjeu qu'il représente, dans un cadre personnel et quotidien bien sûr mais aussi collectif et institutionnel, et notamment en santé.


### Découverte de l'écosystème pharmaceutique et du LGO

Une fois ma curiosité sur le libre assouvie et à mesure que je plongeais dans le monde de P4pillon, mes lacunes en médecine m'ont très vite rattrapées. 
N'étant jamais été, jusque là, intéressé par le sujet, j'ai découvert comment fonctionnait une pharmacie d'officine (en opposition à une pharmacie d'hôpital) et en quoi le LGO est indispensable aux pharmaciens d'officine.  

En effet, s'il y a un élément dont je ne soupçonnais pas l'importance, c'est bien la dimension "petit commerce" d'une pharmacie d'officine. 
Le(s) pharmacien(s) titulaire(s), propriétaire(s) de la pharmacie et responsable(s) de l'ensemble de l'équipe officinale (pharmaciens adjoints, préparateurs, étudiants, secrétaires, etc.), doivent en permanence surveiller leurs stocks, gérer leurs achats auprès des fournisseurs et s'assurer de la rentabilité de leur commerce pour ne pas mettre la clé sous la porte.
Cela se traduit pour un logiciel de gestion d'officine en une solution qui se doit d'être...

* ...**robuste** : une pharmacie se retrouverait contrainte de fermer ses portes en cas de panne de son LGO car l'ensemble de son activité y est lié;
* ...**complète** : un pharmacien et son équipe doivent pouvoir à la fois avoir accès aux dossiers de leur patientèle, orchestrer les arrivées quotidiennes de médicaments et transmettre leurs factures à la sécurité sociale.

Ces deux éléments capitaux étant probablement la raison pour laquelle les pharmacies sont actuellement embourbées dans les méandres d'éditeurs de logiciels propriétaires, qui, certes, récupèrent plus ou moins effrontément les données médicales des patients mais qui proposent des solutions éprouvées et une assistance informatique conséquente, sans aucun doute rassurantes pour un pharmacien.

Pour mieux comprendre ces priorités, deux opportunités de se mettre à la place d'un pharmacien se sont présentées.
La première fut la présentation personnalisée du LGO Winpharma par une pharmacienne en exercice. Elle m'a livré que des quelques logiciels avec lesquels elle avait pu travailler, c'était le plus complet et pratique d'utilisation - ce qui fut unanimement approuvé par les pharmaciens de P4pillon qui connaissait Winpharma. Il fut donc décider de tenter d'également s'inspirer de Winpharma lors de la réimplémentation de Chrysalide.  

De mon côté, j'ai entrevu toute la complexité d'un tel logiciel.
Elle fut très vite confirmée par une deuxième opportunité : la lecture de la thèse d'exercice de Charles Brisset [15], un pharmacien de P4pillon, qui dresse en 2014 un état des lieux toujours d'actualité du monde des LGO en France.
Y sont listés et expliqués l'ensemble des acteurs et des fonctionnalités des LGO - une liste ayant servi de premier ébauche de cahier des charges, se basant sur cette thèse et synthétisant les fonctionnalités d'un LGO est donnée en annexe.  

On découvre également dans cette thèse les parts de marché des différents éditeurs : il y a beau avoir une vingtaine de solutions disponibles, deux groupes (CEGEDIM et Pharmagest interactive) se partagent 75% du marché et l'on monte à plus de 90% en y ajoutant Winpharma.
Ce quasi-monopole n'est pas rassurant mais on notera tout de même que sont également évoqués en dernière partie de la thèse les logiciels libres comme potentiels futurs garants la souveraineté des utilisateurs...


## Discussions d'équipe

Nicolas étant en charge de coordonner l'effervescence de ce début de projet, il m'a confié la conduite de quelques discussions d'équipe concernant des choix préalables au développement du futur LGO. En voici deux exemples, contextualisés et synthétisés.


### Publication immédiate ou différée des données du LGO de P4pillon

Dans le contexte de l'édition d'un logiciel libre dans un marché hautement concurrentiel et très fermé, en atteste les pourcentages de la partie précédente, s'est posée la question de la publication immédiate du code et de la documentation technique du futur LGO.
Cette question structure en effet toute la suite du projet. Ses conséquences peuvent être décisives et irréversibles pour la suite du projet, c'était donc un choix à mûrir pour l'ensemble des membres de l'équipe car les avis divergent.  

En effet, faire un logiciel libre, ça ne serait pas pour "privatiser" des informations, même temporairement : il semblerait difficile de trouver des soutiens crédibles et de la main d'oeuvre chez des libristes endurcis, la communication du projet elle-même pourrait en pâtir.
Pourtant, craignant tantôt un vol, tantôt un sabotage, de la concurrence, certains préféreraient ne rien rendre public avant la sortie d'une première version (V1), voire même d'une V1 avec une masse critique d'utilisateurs. D'autres minimiseront ces risques puisqu'ils vont de paire avec un développement de logiciel libre.
Car oui, la beauté du libre et d'une publication immédiate, c'est qu'une communauté s'organise autour du projet et y contribue d'elle-même... Mais c'est aussi prendre le risque de s'éparpiller et d'avoir plus de travail de gestion de contributions éparses que de réel travail de développement logiciel : cela augmenterait considérablement le temps pour produire une V1 fonctionnelle alors qu'on espérerait par l'augmentation du nombre de contributeurs une diminution de ce temps.  

On pourrait alors continuer en affirmant qu'un manque de clareté due à la "privatisation" de trop d'informations entrainerait un processus de contribution externe trop fastidieux mais on comprend déjà l'essence des discussions qui ont pu avoir lieu : deux argumentaires diamétralement opposés qui ont tous deux leurs raisons d'être, leur validité.  

Finalement, en attendant de se mettre d’accord sur un plan de communication précis - Antoine, porteur et communiquant principal du projet, étant bientôt à nouveau papa, le cahier des charges et plus largement la "documentation pour développeur" sera éditée en Markdown avec les dépôts Git des membres qui l'écrive *(plus d'explication sur cette méthode dans la partie "phase de rédaction")*.
Bien que cette solution soit "publique" puisque celui qui possède l'URL d'un tel dépôt peut accéder à son contenu, elle reste discrète : limitation des risques d'une publication immédiate sans se priver de tous ses avantages.


### Intégrer le LGO à un ERP existant

Un autre sujet de discussion fut la question de l'intégration du LGO à un progiciel de gestion intégré (ERP, de l'anglais "Entreprise Resource Planning").
Un ERP est un logiciel professionnel qui permet de gérer l'ensemble des processus d'une entreprise, notamment la gestion des stocks, des terminaux de paiements ou encore de la comptabilité : autant de fonctionnalités qui devront également figurer dans le LGO.
Il s'agirait alors de se greffer à un ERP libre et d'y développer uniquement une extension pour tous les besoins de la pharmacie d'officine.  

L'intérêt d'une telle intégration semble évident : éviter de coder des fonctionnalités qui existent déjà car utiles à tout type de commerce, réduire la quantité de travail à faire et ainsi gagner du temps sur la première publication du logiciel.
D'autant plus que ce choix semble déjà avoir porté ses fruits pour d'autres logiciels libres en santé, comme le montre l'exemple de GNU Health [16], qui se base sur l'ERP libre Tryton.
L'idée a également été personnellement recommandée à Nicolas par un confrère d'InterHOP [17], un collectif pour la diffusion du logiciel libre en centres hospitaliers - ce que j'ai appris en délivrant cette suggestion à Nicolas.  

Néanmoins, je résumerais cette discussion par la formule "l'idée semble avoir séduit sans avoir convaincu".
Car bien qu'un tel choix puisse procurer un prodigieux gain de temps, certains semblent inquiets du temps qui pourrait être perdu à redévelopper certaines fonctionnalités de l'ERP pour les adapter au mieux au futur LGO.
A cela s'ajoute le risque d'embarquer des fonctionnalités inutiles pour un logiciel de pharmacie, le rendant plus lourd et indigeste que nécessaire.
Aussi le choix de l'ERP semble-t-il décisif dans la décision d'y intégrer, ou non, le futur LGO.
Ont été évoqué Tryton, Odoo et Dolibarr [18], l'alternative la plus sérieuse pour l'instant.  

En conclusion, il semble que la prise de décision en équipe par recherche d'un consensus soit un processus particulièrement laborieux et délicat mais nécessaire au bon développement d'un projet libre, ma courte expérience me permettant déjà de confirmer modestement les propos de Karl Fogel [9].


## Phase de rédaction

Etant, comme expliqué au point précédent, au commencement de ce vaste projet, une autre des grandes missions de mon tuteur, et par conséquent dans laquelle j'ai pu l'assister, fut de rédiger de la documentation.

### Rédaction de quelques éléments de documentation

En effet, il s'agissait de cadrer le développement logiciel à venir, premièrement en présentant facilement le fonctionnement d'un LGO aux informaticiens non pharmaciens de P4pillon.  

Dans le but de faciliter la rédaction du cahier des charges, j'ai par exemple pris des notes sur la thèse de Charles Brisset [15] qui, actuellement, servent de sommaire pour l'écriture du cahier des charges.
En vue d'une réunion, j'ai aussi eu la tâche de rassembler, notamment à travers des textes de lois, le maximum d'informations à propos des agréments nécessaires pour pouvoir publier un LGO.
Enfin j'ai pris des notes lors des réunions et/ou rencontres effectuées, même s'il s'est avéré que Nicolas en prenait également.  

Tous ces fichiers ont été écrits avec Markdown [19], un langage de balisage léger, c'est-à-dire à la syntaxe simple, conçu pour être éditer facilement par Notepad, Gedit, Emacs ou autre éditeur de texte.
Ce choix a d'abord été fait par compatibilité avec les plateformes Nextcloud & PicoCMS mises en place sur les serveurs de P4pillon, puis s'est confirmé avec la simplicité de maniement par un gestionnaire de versions, Git.
Un gestionnaire de version permet de stocker un ensemble de fichiers en conservant la chronologie de toutes les modifications, ce qui permet d'avoir un meilleur recul sur ses projets et de faciliter les corrections - on peut aisément éditer la modification d'il y deux mois qui sabote la version actuelle du projet.
Le dernier avantage et non des moindres est que ce type de logiciel permet de collaborer très efficacement sur un même fichier, de manière centralisée (tous les participants éditent le même fichier) comme décentralisée (chaque participant a sa version du fichier qu'il peut compléter par des modifications des autres).  

Un tel fonctionnement a ainsi permis, dans notre contexte, de conserver un historique du projet, d'obtenir une prévisualisation facile de la documentation et de pouvoir partager les écritures et les relectures de documentation entre les différentes personnes disponibles à un moment donné.


### Rédaction du rapport de stage

Sur suggestions de mon tuteur de stage, ce rapport de stage a été rédigé de manière plutôt originale, sur le modèle de la documentation de P4pillon.
En effet, il a également été rédigé en Markdown et versionné avec Git.
Seuls ont donc été nécessaires pour sa rédaction un terminal de commandes et un éditeur de texte basique, autrement dit le minimum fourni par n'importe quelle distribution GNU/Linux, loin des Microsoft Word, Google Docs ou autre LibreOffice !  

Cependant, si la documentation technique n'a pas besoin d'être transformée, ce fut pourtant le cas de ce rapport.
Aussi fut utilisé un logiciel de conversion de documents numériques en ligne de commande, Pandoc [20], transformant via LaTeX et ses mises en page le fichier Markdown en fichier PDF. La commande
``` bash
$ pandoc -N --toc rapport2stage.md -o rapportMaximeCESSON.pdf
```
permettant une telle transformation avec création automatique de la table des matières.

J'ai entièrement découvert ces moyens d'éditions pendant ce stage et j'ai beaucoup cherché et appelé Nicolas au secours quand j'avais besoin de faire quelque chose de tout à fait inconnu (ajuster l'interligne, ajouter une bibliographie, réaliser une page de garde complète, etc.).
De plus, sans avoir à partager de nombreux bouts de code sur des projets compliqués, j'ai largement enrichi mon utilisation de Git, qui se restreignait grossièrement à charger le code source d'un projet et à partager le mien.
Grâce à Nicolas, j'ai pu y consacrer beaucoup de temps et essayer et tester ce que nous n'avons que le temps de survoler en cours d'ING1 car pas explicitement au programme mais pourtant essentiel pour progresser dans de bonnes conditions.  

Pour conclure, je considère la rédaction de ce rapport comme une partie essentielle de mon stage, au double sens du temps consacré en stage et de ce qu'elle m'a apporté.
En effet, cela m'a permis d'en apprendre plus et de m'exercer sur des outils que je pourrais utiliser quotidiennement à l'avenir, et pas seulement pour la rédaction de documents.
 

# Bilan

Comme cela est longuement décrit dans la partie précédente, j'ai découvert pendant ce stage plein de choses nouvelles par rapport aux cours et projets de cette année.
En effet, par son cadre très particulier *- dans une association de pharmaciens-informaticiens*, je n'aurais pu m'attendre à effectuer un tel stage dès l'ING1.
C'est une vraie chance car, contrairement aux projets pour apprendre, scolaire ou personnel, j'ai été baigné dans la conception d'un projet vaste et ambitieux dont l'impact pourrait être énorme.
Toutes les pharmacies pourraient faire évoluer leur paradigme et s'appuyer sur ce format de logiciel d'ici une dizaine ou une vingtaine d'années.

De plus, même si je pensais que, professionnellement, le statut d'indépendant intervenant sur des missions ponctuelles ne me plairait pas, j'ai par ce stage totalement reconsidéré ce point.
J'ai pu découvrir l'émergence d'une équipe de divers horizons et de ce que cela peut apporter à un projet. 
J'ai pu découvrir les tensions qui pouvaient en émerger mais aussi les solutions qu'on pouvait y apporter.
J'ai pu découvrir les instabilités et les faiblesses mais aussi les souplesses et les forces de telles organisations.

Néanmoins, mon bilan sur ce stage est en demi-teinte car le projet étant très vaste, sa conception est bien plus longue que je n'aurais pu l'imaginer et le fait de s'y atteler aux mois de juillet et d'août n'a rien arrangé. 
L'association, uniquement composée de bénévoles, est, en effet, à la fois commanditaire et conceptrice de ce nouveau LGO. 
Or il s'est imposé assez tôt que ces deux facettes ne pouvaient que difficilement se mélanger, et notamment qu'un cahier des charges complet et détaillé se devaient de voir le jour avant tout. 

Cette rédaction de cahier des charges n'étant le fruit que du travail de mon tuteur, elle prend donc naturellement beaucoup de temps, d'autant que la quantité de travail à fournir est grande (se référer à l'annexe *Notes sur les fonctionnalités d'un LGO* qui en détaille une ébauche de plan).
Mi-août, seules quelques pages ont d'hors et déjà été finalisées malgré un nombre important déjà en préparation.
Antoine, le président, a même convié les développeurs de l'association les 14 et 15 octobre prochain, à l'occasion d'un événement autour du logiciel libre en région Nouvelle-Aquitaine, le b-boost [21], à se réunir pour commencer à travailler en profondeur sur le LGO, qui, au plus tôt pourrait voir le jour en milieu d'année prochaine.

Ç'aurait été impossible au vue du contexte mais si c'était à refaire autrement, j'essaierai d'avoir un rôle plus actif, notamment au niveau des tâches techniques.
Car j'ai, à regret, très peu codé.
Aussi aurais-je certainement pu être plus clair avec Nicolas en début de stage en lui précisant mieux mes compétences en développement, si tant est qu'il y ait vu une utilité quelque part. 

Bien que j'ai pu confirmer l'importance que je porte au fait d'effectuer une majorité de missions techniques par rapport à des missions d'autres types dans mon projet professionnel, j'ai tout de même largement pratiqué et développé mes compétences dans des domaines connexes, ainsi mes sentiments à l'égard de ce stage sont loins d'être négatifs.
Par ailleurs, Nicolas a toujours été très transparent, souple, m'a expliqué plein de sujets issus de la pharmacie et du libre et m'a fourni de nombreux conseils sur tous les fronts.

Plus qu'une expérience professionnelle parmi d'autres, je pense que je cherchais dans ce stage une confirmation de ma projection personnelle du métier d'ingénieur et d'informaticien. 
Le besoin de trouver un sens dans son travail et de partager des valeurs communes m'ont donc elles aussi été confirmées grâce à ce stage et m'ont immédiatement fait adhérer à l'association P4pillon dans laquelle j'aurais très certainement postulé si j'avais eu connaissance d'une offre de stage en bonne et due forme. 


# Conclusion

Ce stage à l'association P4pillon fut pour moi une réelle chance et restera une expérience enrichissante.
J'ai d'abord eu l'occasion de faire un stage dans une association dans un premier temps, à propos d'un sujet brûlant dans un second temps et en expérimentant des manières de travailler, de concevoir et de s'organiser ancrées dans le libre dans un dernier temps.

Ensuite et pour rebondir sur les points évoqués dans la description des missions effectuées, j'ai appris beaucoup sur des sujets que j'ignorais, et que, sans ce stage, j'aurais sans nul doute continuer d'ignorer : des convergences entre le monde du libre et ceux de la médecine et de la pharmacie, des manières de s'organiser pour formuler des travaux académiques type mémoire ou thèse mais aussi des moyens de rédiger de façon simple, claire et efficace tout ce qui a besoin de l'être sous Linux !  

Par la découverte de nombreux projets, j'ai aussi grandement élargi ma culture du logiciel libre et, grâce à Nicolas, développé mon réseau professionnel dans ce milieu qui me captive. 
Au niveau des compétences techniques, c'est certainement sur le logiciel de version Git que je me suis le plus amélioré, ce qui saura plaîre, j'en suis convaincu, à de futurs employeurs et me sera utile dans ma vie d'informaticien.
Quant aux compétences transversales, j'ai pu m'exercer à la prise de notes et à la synthèse de documents dont j'ai l'habitude puis découvrir, à l'écrit, la conduite de discussions d'équipe.
Cette dernière visant à orienter les débats et à être capable de comprendre et de prendre en considération l'ensemble des avis, il s'agit d'une expérience qui me sera, j'en suis convaincu là aussi, très utile lors de mes premières réunions professionnelles. 

Bien que j'ai éprouvé quelques regrets quant aux tâches techniques non effectuées, ce stage n'a pas invalidé mon projet professionnel, au contraire, il l'a même élargi par rapport au cadre de travail.
Travailler dans une petite structure pas forcément bien huilée où on peut s'occuper de tâches quelques fois éloignées de son coeur de métier, parce que l'on aime ce que l'on fait et les valeurs qu'on diffuse, ne me fait plus peur.
Car s'il y a bien une chose qui m'ait été confirmé par tout l'optimisme de ce projet P4pillon, c'est la volonté de travailler pour quelque chose qui a du sens, qui incarne mes convictions personnelles, qui me rende fier d'être à ma place.

Aussi resterais-je, je pense, sensible à la cause du libre pour plusieurs années, jusqu'à peut-être un jour une perspective d'embauche...


# Annexes

## Notes sur les fonctionnalités d'un LGO

D'après la lecture de la thèse Charles Brisset [15].

### Fonctionnalités obligatoires (légales)

1. **Ordonnancier**

   * "stock" des ordonnances 
2. **Dossier pharmaceutique (DP)**
   * Quid du DMP (Dossier Médical Partagé) ?
3. **Base de données des médicaments et des produits de santé**
   * Dictionnaire Vidal, Base Claude Bernard, Thériaque, autre alternative libriste ?
4. **Tiers-Payant** (SESAM-Vitale, ADELI, NOEMIE, carte Vitale + carte de professionel de santé (CPS), SCOR)
   * SCOR: scannérisation des ordonnances (phase de transmission)


### Fonctionnalités indispensables

1. **Gestion du stock :** com' avec les grossistes (via PharmaML), lecture code barre 2D (format Datamatrix).
2. **Gestion des fiches patients & practiciens :** transmission des infos ordonnance aux assurances maladies.
3. **Comptabilité :** a minima export vers un logiciel compétent (ex: GNUCash?), mieux si directement intégré.
4. **Système de gestion des droits :** restrictions des accès (ex: titulaire =?root, pharmacien, préparateur, respo stock, magasinier) en fonction des postes.
5. **Contrôle de la mutuelle (patients) :** système de carte à puce ?


### Fonctionnalités optionnelles

 1. Rédaction de fiches d'opinion pharmaceutique
 2. Achat de médicaments aux grossistes par groupement d'officines (négocier des prix). /!\\ *Scalabilité :* pas besoin d'être *tous* sur le même LGO pour commander à plusieurs.
 3. Gestion des collectivités et de leurs formes variées de facturation
 4. Télétravail -> titulaire peut de chez lui permettre certaines opérations par ses accès réservés (mise en place d'un VPN)
 5. Possibilité d'envoyer un SMS lors de la réception d'une commande d'un patient
 6. Gestion de programmes de fidélité pour la parapharmacie
 7. Location de matériel médical (lit, béquilles, etc.)
 8. Gestion des EDT de l'équipe officinale (à coupler avec les informations sur la fréquentation de la pharmacie)
 9. Gestion de la caisse (en complément de l'option "comptabilité")
10. Aide à la préparation des doses à administrer (EHPAD)
11. Comparateur de prix de la parapharmacie
12. Gestion des sauvegardes de l'ordonnancier (quasi depuis le début d'exercice - solutions locales & distantes)
13. Modification des étiquettes électroniques (signal infrarouge)
14. Gestion terminaux paiement électronique (faut fournir le TPE)
15. Fiches conseils à disposition des patients
16. Notes sur un patient, un practicien ou un "prestataire"
17. Remonter facilement les cas de pharmacoviligance
18. Modifier les messages affichées sur la croix LED extérieure
19. Remontée des issues techniques et possibilité de prise en main à distance
20. Manuels d'utilisation du LGO et des différentes fonctionnalités
21. Intégration d'un futur système de messagerie sécurisée de santé
22. Intégration d'un futur système avec caméra
23. Statistiques sur les ventes 

En addition de tous ces points : tout ce qui peut être utile et faire gagner du temps au pharmacien (voir avec les utilisateurs de Winpharma si certaines fonctionnalités sont particulièrement appréciées)


## CV


# Bibliographie - Webographie

[1] Cash Investigation, **Nos données personnelles valent de l'or !** *[retransmission d'une édition du magazine de France 2]*. France Télévision - 20/05/2021, https://www.france.tv/france-2/cash-investigation/2450927-nos-donnees-personnelles-valent-de-l-or.html, https://peertube.underworld.fr/videos/watch/a4f03cab-999a-4d90-bc2b-0e49693a227e  
[2] FLOQUET Nicolas, **Libre et santé : comprendre le logiciel libre et ses enjeux pour les professions de santé**, Thèse d'exercice pharmacie de Lille 2 - 2014,  https://www.sudoc.fr/18121816X *[consulté en ligne]*  
[3] P4pillon, **Construisons l'avenir du système de santé** *[projet associatif]*. https://www.p4pillon.org  
[4] HOOD Leroy, **Systems biology and p4 medicine: past, present, and future**. Publié dans le *Rambam Maimonides Medical Journal* - 2013, https://pubmed.ncbi.nlm.nih.gov/23908862/  *[consulté en ligne]*  
[5] PRIOUX Antoine, DE VENDEUVRE Stéphane, **Les entretiens de Théragora - Interview d’Antoine Prioux par Stéphane de Vendeuvre** *[vidéo en ligne]*. Viméo, *Télé Millevaches* - 20/05/2021, https://vimeo.com/552828297  
[6] Open Source Initiative (OSI), **Licenses & Standards**, *[association étatsunienne reconnue d'utilité publique]*. https://opensource.org/licenses  
[7] LEVY Steven, **L'éthique des hackers**. Globe - 2013, ISBN 978-2-211-20410-1.  
[8] WILLIAMS Sam, STALLMAN Richard, MASUTTI Christophe, **Richard Stallman et la révolution du logiciel libre**. Eyrolles - 2010, https://framabook.org/richard-stallman-et-la-revolution-du-logiciel-libre-2/  
[9] FOGEL Karl, **Produire du logiciel libre**. In Libro Veritas - 2010, ISBN  978-2-35922-034-6, https://framabook.org/produire-du-logiciel-libre-2/  
[10] e Foundation, **/e/, un écosystème mobile, complet et entièrement « déGooglisé »** *[projet associatif]*. https://e.foundation/ *[consulté le 18/06/2021]*  
[11] La Brique Internet, **Bâtissons ensemble un Internet libre, neutre et décentralisé** *[projet associatif]*. https://labriqueinter.net/ *[consulté le 16/06/2021]*  
[12] BAYART Benjamin, **Internet Libre ou Minitel 2.0 ?** *[conférence en ligne]*. Association FDN (French Data Network) - 07/2007, https://www.fdn.fr/actions/confs/internet-libre-ou-minitel-2-0/  
[13] April, **Promouvoir et défendre le logiciel libre** *[association française]*. https://www.april.org/  
[14] Free Software Foundation, **Working together for free software** *[association soutenant le projet GNU/Linux]*. https://www.fsf.org/, https://www.gnu.org/  
[15] BRISSET Charles, **Les logiciels de gestion d'officine : fonctionnalités et acteurs**, Thèse d'exercice pharmacie de Poitiers - 2014, https://www.sudoc.fr/184134951 *[consulté en ligne]*  
[16] GNU Health, **Freedom and Equity in Healthcare**. GNU Solidario *[association humanitaire]*, https://gnuhealth.org/  
[17] Interhop, **Logiciels libres, interopérabilité et utilisation auto-gérée des données** *[collectif issu de centres hospitaliers]*. https://interhop.org/  
[18] Dolibarr, **ERP & CRM Open Source**. Dolibarr foundation *[association française]*, https://www.dolibarr.org/  
[19] Markdown, **Langage de balisage léger**. https://fr.wikipedia.org/wiki/Markdown/  
[20] Pandoc, **Logiciel libre de conversion de documents numériques**. https://pandoc.org/  
[21] PRIOUX Antoine, **B-boost** *[convention d'opensource à venir]*. Nouvelle-Aquitaine Open Source et le Conseil National du Logiciel Libre - 14&15/10/2021, https://www.b-boost.fr  