# Chronologie du stage

## Juin

#### Mardi 15 juin :
- Réunion entre inter’hop et p4pillon (presque) sans inter’hop

#### Mercredi 16 juin :
- Découverte des outils de discussion et de documentation (cloud.p4pillon.org)
- Première préparation espace de travail (dossiers PC + tableur pour tâches et heures)
- Visionnage d'une conférence de la FFDN et visites d'autres sites internet
- Discussions avec Nicolas

#### Jeudi 17 juin :
- Tentative d'installation de médiboard sur un serveur xampp local

#### Vendredi 18 juin :
- Visites de nombreux sites internet en rapport avec le monde libre sur conseils de Nicolas
- Début du visionnage de l'interview d'Antoine Prioux (président P4pillon) par Théragora

#### Lundi 21 juin :
- Visite de la pharmacie de la Ferrière-aux-Etangs et de son LGO (Winpharma)
- Collecte de la documentation proposée par Nicolas
- Préparation des dépôts git local et distant (gitlab.com) pour mon rapport de stage (+ clé SSH)

#### Mardi 22 juin :
- Lecture et prise de notes sur la thèse de Charles à propos des LGO (notes sur les parties "fonctionnalités")
- Initiation au langage Markdown

#### Mercredi 23 juin :
- Suite et début de prise de notes sur l'interview d'Antoine Prioux par Théragora
- Rencontre physique avec Nicolas & les écuyers du hack à Lonlay l’Abbaye

#### Jeudi 24 juin :
- Fin du visionnage et de la prise de notes sur l'interview d'Antoine Prioux par Théragora

#### Vendredi 25 juin :
- Rencontre physique avec Nicolas & Emmanuel : discussion sur les technologies à utiliser pour le futur LGO
- Rédaction mini CR (un CR alternatif est finalement publié par Nicolas)

#### Lundi 28 juin :
- Lecture des avis variés publiés pendant le WE sur les technologies à choisir pour le LGO de P4pillon
- Appropriation du concept de WebApp
- Découverte des attendus du rapport de stage
- 2e tour dans Markdown

#### Mardi 29 juin :
- Rédaction de la structure du rapport de stage
- Réunion avec une partie de l'équipe de P4pillon sur le choix des technologies
- Partage de mes notes sur la thèse de Charles pour une ébauche de cahier des charges

#### Mercredi 30 juin :
- Rédaction et mise en forme d'une bibliographie / webographie pour le rapport de stage

## Juillet

#### Jeudi 1 juillet :
- RAS

#### Vendredi 2 juillet :
- RAS

#### Lundi 5 juillet :
- Lecture des nouvelles des jours précédents et échanges avec Nicolas
- Découverte d'un usage décentralisé de GIT
- Organisation RDV bilan intermédiaire entre Nicolas, C. Elo et moi

#### Mardi 6 juillet :
- Entraînement dans mon usage de GIT (prise en compte sélective de modifications légères de Nicolas sur le rapport)
- Découverte de nouvelles commandes GIT
- Echange avec Nicolas sur le format du rapport de stage : découverte de l'utilisation de fichiers markdown avec pandoc

### Mercredi 7 juillet :
- Exploration des fonctionnalités proposées par pandoc et des mises en page associées
- Live coding session by Emmanuel
- Reformatage du rapport de stage à l'écrit
- Passage à GIT : séparation granulaire des modifications variées sur le rapport (ajout partiel)

### Jeudi 8 juillet :
- Création de ce fichier de chronologie sur demande de Nicolas et complétion jusqu'à aujourd'hui
- Réunion bilan interédiaire entre Nicolas, C. Elo et moi
- Discussion avec ATILLA à propos de la publication du code avant une première version du LGO (+ CR Nicolas)

### Vendredi 9 juillet :
- Echanges avec Nicolas : m'est confiée la discussion autour de la publication immédiate du code du LGO
- Rédaction rapport de stage (introduction)

### Lundi 12 juillet :
- Proposition d'intégration du LGO à un ERP : m'est confiée une discussion autour de la question
- Long échanges avec Arthur sur la publication des données et sur le libre
- Recherche sur les agréments à obtenir pour publier un LGO en France

### Mardi 13 juillet :
- Suite des échanges des jours précédents
- Réunion avec Nicolas
- Réunion avec toute l'équipe : Antoine annonce la participation de P4pillon au b-boost et un bilan est dressé sur l'audit du code du précédent LGO

### Jeudi 15 juillet :
- Rédaction rapport de stage (présentation entreprise)
- Quelques rectifications sur l'ensemble du rapport
- MAJ de cette chronologie
- Partage de ces informations avec Nicolas (GIT)


## Août
