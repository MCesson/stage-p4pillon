#!/bin/bash

# transforme le markdown en pdf
pandoc -N --toc rapport2stage.md -o rapport2stage.pdf

# retire la page de garde et les deux dernières pages de bibliographie
nbPage=`pdftk rapport2stage.pdf dump_data | grep "NumberOfPages" | cut -d " " -f 2`
pdftk rapport2stage.pdf cat 2-$(($nbPage-2)) output corps.pdf

# récupère les deux dernières pages de bibliographie
pdftk rapport2stage.pdf cat $(($nbPage-1))-$nbPage output biblio.pdf

# transforme la page de garde personnalisée (LaTeX) en pdf
pdflatex page2garde.tex &> /dev/null

# ajoute la page de garde avant et le CV puis la bibliographie après
pdftk page2garde.pdf corps.pdf CV_CESSON.pdf biblio.pdf cat output rapportMaximeCESSON.pdf

# retire les fichiers inutiles
rm rapport2stage.pdf corps.pdf biblio.pdf page2garde.pdf page2garde.aux page2garde.log